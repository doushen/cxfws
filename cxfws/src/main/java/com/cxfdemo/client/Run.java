package com.cxfdemo.client;

import javax.servlet.ServletConfig;

import org.apache.cxf.Bus;
import org.apache.cxf.BusFactory;
import org.apache.cxf.frontend.ServerFactoryBean;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;
import org.apache.cxf.transport.servlet.CXFNonSpringServlet;

import com.cxfdemo.server.Greeting;
import com.cxfdemo.server.GreetingImpl;
import com.cxfdemo.server.HDService;
import com.cxfdemo.server.HDServiceImpl;

public class Run extends CXFNonSpringServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void loadBus(ServletConfig sc) {
		super.loadBus(sc);

		Bus bus = getBus();
		BusFactory.setDefaultBus(bus);

		// 发布WS 用ServerFactoryBean 参数名默认是 arg0....argN
		ServerFactoryBean sfb = new ServerFactoryBean();//参数名为arg0
		sfb.setServiceClass(Greeting.class);
		sfb.setAddress("/Greeting");
		sfb.setServiceBean(new GreetingImpl());
		sfb.create();
		// 发布WS 用JaxWsServerFactoryBean可以指定参数名
		JaxWsServerFactoryBean jwsfb = new JaxWsServerFactoryBean();//参数名为指定值
		jwsfb.setServiceClass(HDService.class);
		jwsfb.setAddress("/HDService");
		jwsfb.setServiceBean(new HDServiceImpl());
		jwsfb.create();
	}
}
