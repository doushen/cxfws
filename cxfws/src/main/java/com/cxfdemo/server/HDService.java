package com.cxfdemo.server;


/*
 * 定义WS服务接口
 * 
 * */
import javax.jws.WebParam;
import javax.jws.WebService;   
@WebService
public interface HDService {
	public String BillStatusUpd(@WebParam(name = "billno") String billno,
			@WebParam(name = "status") String status);
}
